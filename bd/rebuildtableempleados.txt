delete from empleados;

INSERT INTO empleados (cif, nombre, apellidos, edad) VALUES
("34334789H", "Antonio", "Martín", 23),
("21094387T", "Juan", "González", 40),
("01293474E", "Isabel", "Fuentes", 18),
("23948745F", "Lucille", "King", 25),
("40948574G", "Neo", "Preno", 12);